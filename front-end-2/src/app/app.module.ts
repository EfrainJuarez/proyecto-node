import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { IndexComponent } from './components/index/index.component';
import { FormulariosComponent } from './components/formularios/formularios.component';
import { ProductoComponent } from './components/vistas/producto/producto.component';

@NgModule({
  declarations: [AppComponent, IndexComponent, FormulariosComponent, ProductoComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
