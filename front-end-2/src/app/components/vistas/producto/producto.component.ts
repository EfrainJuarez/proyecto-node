import { Component, OnInit } from '@angular/core';
import { AppServiceService } from 'src/app/Servicio/app-service.service';
import * as alertify from 'alertifyjs';
import { Router } from '@angular/router';

import { FormulariosComponent } from '../../formularios/formularios.component';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  template: ` <app-formularios [idrecibir]="idenviar"></app-formularios> `,
  styleUrls: ['./producto.component.css'],
})
export class ProductoComponent implements OnInit {
  idenviar: any;
  actualizar: boolean = false;
  vacios = false;
  numero = false;
  texto = false;
  insertarPersonal: boolean = false;
  mostrar: boolean = false;
  data: any = [];
  datosF = {
    nombreProducto: '',
    marcaProducto: '',
    presntacionProducto: '',
    contenidoProducto: '',
    costoProducto: '',
    proveedorProducto: '',
    cantidadProducto: '',
    statusProducto: '',
    descripcionProducto: '',
  };
  constructor(private router: Router, private _apiService: AppServiceService) {}

  ngOnInit(): void {
    const modelName = 'producto';
    this._apiService.consultar(modelName).subscribe((resp: any) => {
      this.data = Object.values(resp.respuesta);
      if (this.insertarPersonal) {
        this.mostrar = false;
      } else {
        this.mostrar = true;
      }
    });
  }

  btninsertar() {
    this.insertarPersonal = !this.insertarPersonal;
    this.mostrar = !this.mostrar;
    this.Limpiar();
  }
  btnactualizar() {
    this.actualizar = !this.actualizar;
  }

  actualizar_form(id: any) {
    this.actualizar = !this.actualizar;
    this.idenviar = id;
    this.router.navigate(['/formulario', this.idenviar]);
    console.log(this.idenviar);
  }
  registroUser() {
    const modelName = 'producto';
    let elementsRegistro = document.querySelectorAll('.ingresar');
    this.validar(elementsRegistro, 'ingresar');
    if (this.numero == true || this.texto == true || this.vacios == true) {
      //restablecemos el valor de estas para volver a validad nuevamente si es necesario
      this.texto = false;
      this.numero = false;
      this.vacios = false;
      return;
    } else {
      this._apiService
        .agregar(modelName, this.datosF)
        .subscribe((resp: any) => {
          alertify.alert('Producto insertado con exito', function () {});
          this.consultarUser();
        });
      this.Limpiar();
      this.mostrar = !this.mostrar;
      this.insertarPersonal = !this.insertarPersonal;
    }
  }

  eliminarPorBusqueda(id: any) {
    const modelName = 'producto';
    alertify.confirm(
      'Desea eliminar el registro?',
      () => {
        this._apiService.eliminar(modelName, id).subscribe((resp: any) => {
          alertify.alert('Producto borrado con exito', function () {});
          this.consultarUser();
        });
      },
      function () {
        alertify.error('Cancel');
      }
    );
  }

  consultarUser() {
    const modelName = 'producto';
    this._apiService.consultar(modelName).subscribe((resp: any) => {
      this.data = Object.values(resp.respuesta);
      if (this.insertarPersonal) {
        this.mostrar = false;
      } else {
        this.mostrar = true;
      }
    });
  }

  Limpiar() {
    const Input = document.getElementsByTagName('input');
    for (let i = 0; i < Input.length; i++) {
      Input[i].value = '';
    }
    this.vaciarDatosUsuario();
  }
  vaciarDatosUsuario() {
    this.datosF.nombreProducto = '';
    this.datosF.marcaProducto = '';
    this.datosF.presntacionProducto = '';
    this.datosF.contenidoProducto = '';
    this.datosF.costoProducto = '';
    this.datosF.proveedorProducto = '';
    this.datosF.cantidadProducto = '';
    this.datosF.statusProducto = '';
    this.datosF.descripcionProducto = '';
  }

  //Validaciones
  validarVacios = (elementos: any) => {
    for (let i = 0; i < elementos.length; i++) {
      if (elementos[i].value == '') {
        this.vacios = true;
        alert('Existen campos vacios, verifique');
        return;
      }
    }
  };
  validarTiposDato = (campo: any, tipo: any) => {
    let contenidoCampo = campo.value;

    if (tipo === 'texto') {
      for (let caracter of contenidoCampo) {
        if (caracter.charCodeAt(0) >= 48 && caracter.charCodeAt(0) <= 57) {
          alert(
            `El campo ${campo.getAttribute(
              'id'
            )} contiene números, por favor verifique`
          );
          campo.value = '';
          this.numero = true;
          break;
        }
      }
    } else if (tipo === 'numero') {
      for (let caracter of contenidoCampo) {
        if (caracter.charCodeAt(0) > 57 || caracter.charCodeAt(0) < 45) {
          alert(
            `El campo ${campo.getAttribute(
              'id'
            )} solo debe contener números, por favor verifique`
          );
          campo.value = '';
          this.texto = true;
          break;
        }
      }
    }
  };

  validar = (arrayElements: any, className: any) => {
    let elementsRegistro = document.querySelectorAll(`.${className}`);
    this.validarVacios(elementsRegistro);

    for (let i = 0; i < elementsRegistro.length; i++) {
      if (elementsRegistro[i].classList.contains('Texto')) {
        this.validarTiposDato(elementsRegistro[i], 'texto');
      }
      if (elementsRegistro[i].classList.contains('Numero')) {
        this.validarTiposDato(elementsRegistro[i], 'numero');
      }
    }
  };
}
