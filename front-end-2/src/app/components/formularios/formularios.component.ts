import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppServiceService } from 'src/app/Servicio/app-service.service';
import * as alertify from 'alertifyjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formularios',
  templateUrl: './formularios.component.html',
  styleUrls: ['./formularios.component.css'],
})
export class FormulariosComponent implements OnInit {
  data: any = [];
  datosF = {
    _id: '',
    nombreProducto: '',
    marcaProducto: '',
    presntacionProducto: '',
    contenidoProducto: '',
    costoProducto: '',
    proveedorProducto: '',
    cantidadProducto: '',
    statusProducto: '',
    descripcionProducto: '',
  };
  vacios = false;
  numero = false;
  texto = false;

  @Input() idrecibir: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private AppServiceService: AppServiceService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.idrecibir = params['id'];
      console.log(this.idrecibir);
      this.obtenerID();
    });
  }

  navigateTo(route: string) {
    this.router.navigate([route]);
  }

  obtenerID() {
    const modelName = 'producto';
    this.AppServiceService.consultaID(modelName, this.idrecibir).subscribe(
      (resp: any) => {
        this.data = Object.values(resp.respuesta);
        console.log(this.data);
      }
    );
  }

  actualizarUser() {
    let elementsRegistro = document.querySelectorAll('.ingresar');
    this.validar(elementsRegistro, 'ingresar');
    if (this.numero == true || this.texto == true || this.vacios == true) {
      //restablecemos el valor de estas para volver a validad nuevamente si es necesario
      this.texto = false;
      this.numero = false;
      this.vacios = false;
      return;
    } else {
      this.datosF = {
        _id: this.data[0],
        nombreProducto: this.data[1],
        marcaProducto: this.data[2],
        presntacionProducto: this.data[3],
        contenidoProducto: this.data[4],
        costoProducto: this.data[5],
        proveedorProducto: this.data[6],
        cantidadProducto: this.data[7],
        statusProducto: this.data[8],
        descripcionProducto: this.data[9],
      };

      alertify.confirm(
        'Confirm Message',
        () => {
          const modelName = 'producto';
          let elementsRegistro = document.querySelectorAll('.modificar');
          this.AppServiceService.actualizar(
            modelName,
            this.datosF,
            this.data[0]
          ).subscribe((resp: any) => {});
          this.navigateTo('producto');
        },
        function () {
          alertify.error('Cancel');
        }
      );
    }
  }

  Limpiar() {
    const Input = document.getElementsByTagName('input');
    for (let i = 0; i < Input.length; i++) {
      Input[i].value = '';
    }
    this.vaciarDatosUsuario();
  }
  vaciarDatosUsuario() {
    this.datosF.nombreProducto = '';
    this.datosF.marcaProducto = '';
    this.datosF.presntacionProducto = '';
    this.datosF.contenidoProducto = '';
    this.datosF.costoProducto = '';
    this.datosF.proveedorProducto = '';
    this.datosF.cantidadProducto = '';
    this.datosF.statusProducto = '';
    this.datosF.descripcionProducto = '';
  }

  //Validaciones
  validarVacios = (elementos: any) => {
    for (let i = 0; i < elementos.length; i++) {
      if (elementos[i].value == '') {
        this.vacios = true;
        alert('Existen campos vacios, verifique');
        return;
      }
    }
  };
  validarTiposDato = (campo: any, tipo: any) => {
    let contenidoCampo = campo.value;

    if (tipo === 'texto') {
      for (let caracter of contenidoCampo) {
        if (caracter.charCodeAt(0) >= 48 && caracter.charCodeAt(0) <= 57) {
          alert(
            `El campo ${campo.getAttribute(
              'id'
            )} contiene números, por favor verifique`
          );
          campo.value = '';
          this.numero = true;
          break;
        }
      }
    } else if (tipo === 'numero') {
      for (let caracter of contenidoCampo) {
        if (caracter.charCodeAt(0) > 57 || caracter.charCodeAt(0) < 45) {
          alert(
            `El campo ${campo.getAttribute(
              'id'
            )} solo debe contener números, por favor verifique`
          );
          campo.value = '';
          this.texto = true;
          break;
        }
      }
    }
  };

  validar = (arrayElements: any, className: any) => {
    let elementsRegistro = document.querySelectorAll(`.${className}`);
    this.validarVacios(elementsRegistro);

    for (let i = 0; i < elementsRegistro.length; i++) {
      if (elementsRegistro[i].classList.contains('Texto')) {
        this.validarTiposDato(elementsRegistro[i], 'texto');
      }
      if (elementsRegistro[i].classList.contains('Numero')) {
        this.validarTiposDato(elementsRegistro[i], 'numero');
      }
    }
  };
}
