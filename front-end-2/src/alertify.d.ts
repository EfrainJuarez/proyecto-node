declare module 'alertifyjs' {
  function alert(message: string, okCallback?: () => void): any;
  function confirm(
    message: string,
    okCallback?: () => void,
    cancelCallback?: () => void
  ): any;
  function success(message: string): any;
  function error(message: string): any;
  function log(message: string): any;
}
