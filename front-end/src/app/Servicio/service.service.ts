import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  public url;
  constructor(private _http: HttpClient) {
    this.url = 'http://localhost:3900';
  }
  mostrarMSJ() {
    console.log('ya consulta tu primer servicio');
  }

  consultar = (modelName: string) => {
    let url = `${this.url}/obtener/${modelName}`;
    return this._http.get(url);
  };
  agregar = (modelName: string, body: any) => {
    let url = `${this.url}/${modelName}/nuevo`;
    return this._http.post(url, body);
  };

  actualizar = (modelName: string, body: any, id: any) => {
    let url = `${this.url}/update/${modelName}/${id}`;
    return this._http.put(url, body);
  };

  eliminar = (modelName: string, id: any) => {
    let url = `${this.url}/delete/${modelName}/${id}`;
    return this._http.delete(url);
  };
}
