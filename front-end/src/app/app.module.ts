import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ComponentePrincipalComponent } from './components/index/componente-principal/componente-principal.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentePersonalComponent } from './components/personal/componente-personal/componente-personal.component';
import { ComponenteProductosComponent } from './components/productos/componente-productos/componente-productos.component';
import { ComponenteSucursalComponent } from './components/sucursal/componente-sucursal/componente-sucursal.component';
import { ComponenteProveedorComponent } from './components/proveedor/componente-proveedor/componente-proveedor.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentePrincipalComponent,
    ComponentePersonalComponent,
    ComponenteProductosComponent,
    ComponenteSucursalComponent,
    ComponenteProveedorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
