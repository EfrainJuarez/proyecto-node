import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';

import { ComponentePrincipalComponent } from './components/index/componente-principal/componente-principal.component';
import { ComponentePersonalComponent } from './components/personal/componente-personal/componente-personal.component';
import { ComponenteProductosComponent } from './components/productos/componente-productos/componente-productos.component';
import { ComponenteSucursalComponent } from './components/sucursal/componente-sucursal/componente-sucursal.component';
import { ComponenteProveedorComponent } from './components/proveedor/componente-proveedor/componente-proveedor.component';

const routes: Routes = [
  { path: '', redirectTo: '/index', pathMatch: 'full' },
  { path: 'index', component: ComponentePrincipalComponent },
  { path: 'personal', component: ComponentePersonalComponent },
  { path: 'producto', component: ComponenteProductosComponent },
  { path: 'sucursal', component: ComponenteSucursalComponent },
  { path: 'proveedor', component: ComponenteProveedorComponent },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)], // Agregar RouterModule aquí
  exports: [RouterModule],
})
export class AppRoutingModule {}
