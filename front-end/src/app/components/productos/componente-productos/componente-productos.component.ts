import { Component } from '@angular/core';
import { ServiceService } from 'src/app/Servicio/service.service';

@Component({
  selector: 'app-componente-productos',
  templateUrl: './componente-productos.component.html',
  styleUrls: ['./componente-productos.component.css'],
})
export class ComponenteProductosComponent {
  estadoIngresar = false;
  estadoMostrar = false;
  vacios = false;
  numero = false;
  texto = false;
  estadoActualizar = false;
  data: any = [];
  datosF = {
    nombreProducto: '',
    marcaProducto: '',
    presntacionProducto: '',
    contenidoProducto: '',
    costoProducto: '',
    proveedorProducto: '',
    cantidadProducto: '',
    statusProducto: '',
    descripcionProducto: '',
  };
  idUsuario = '';
  constructor(private _apiService: ServiceService) {}

  ngOnInit(): void {}
  registroUser() {
    const modelName = 'producto';
    let elementsRegistro = document.querySelectorAll('.ingresar');
    this._apiService.agregar(modelName, this.datosF).subscribe((resp: any) => {
      alert('Usuario registrado');
    });
    this.Limpiar();
  }

  consultarUser() {
    const modelName = 'producto';
    this._apiService.consultar(modelName).subscribe((resp: any) => {
      this.data = Object.values(resp.respuesta);
    });
  }

  eliminarPorBusqueda(id: any) {
    const modelName = 'producto';
    this._apiService.eliminar(modelName, id).subscribe((resp: any) => {
      alert('Usuario borrado');
      this.consultarUser();
    });
  }

  actualizarUser() {
    const modelName = 'producto';
    let elementsRegistro = document.querySelectorAll('.modificar');
    this._apiService
      .actualizar(modelName, this.datosF, this.idUsuario)
      .subscribe((resp: any) => {
        alert('Usuario actualizado');
      });
    this.Limpiar();
  }

  Limpiar() {
    const Input = document.getElementsByTagName('input');
    for (let i = 0; i < Input.length; i++) {
      Input[i].value = '';
    }
    this.vaciarDatosUsuario();
  }
  vaciarDatosUsuario() {
    this.datosF.nombreProducto = '';
    this.datosF.marcaProducto = '';
    this.datosF.presntacionProducto = '';
    this.datosF.contenidoProducto = '';
    this.datosF.costoProducto = '';
    this.datosF.proveedorProducto = '';
    this.datosF.cantidadProducto = '';
    this.datosF.statusProducto = '';
    this.datosF.descripcionProducto = '';
  }

  mostrar(num: any) {
    switch (num) {
      case 1:
        if (this.estadoIngresar == false) {
          this.estadoIngresar = true;
          this.estadoActualizar = false;
          this.estadoMostrar = false;
        } else this.estadoIngresar = false;
        break;
      case 2:
        if (this.estadoMostrar == false) {
          this.estadoMostrar = true;
          this.estadoActualizar = false;
          this.estadoIngresar = false;
        } else this.estadoMostrar = false;
        break;
      case 3:
        if (this.estadoActualizar == false) {
          this.estadoActualizar = true;
          this.estadoIngresar = false;
          this.estadoMostrar = false;
        } else this.estadoActualizar = false;
        break;
    }
  }
}
