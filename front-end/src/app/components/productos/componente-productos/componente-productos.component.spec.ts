import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteProductosComponent } from './componente-productos.component';

describe('ComponenteProductosComponent', () => {
  let component: ComponenteProductosComponent;
  let fixture: ComponentFixture<ComponenteProductosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteProductosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponenteProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
