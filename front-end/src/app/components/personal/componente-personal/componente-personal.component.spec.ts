import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentePersonalComponent } from './componente-personal.component';

describe('ComponentePersonalComponent', () => {
  let component: ComponentePersonalComponent;
  let fixture: ComponentFixture<ComponentePersonalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentePersonalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponentePersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
