import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteSucursalComponent } from './componente-sucursal.component';

describe('ComponenteSucursalComponent', () => {
  let component: ComponenteSucursalComponent;
  let fixture: ComponentFixture<ComponenteSucursalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteSucursalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponenteSucursalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
