import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteProveedorComponent } from './componente-proveedor.component';

describe('ComponenteProveedorComponent', () => {
  let component: ComponenteProveedorComponent;
  let fixture: ComponentFixture<ComponenteProveedorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteProveedorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponenteProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
