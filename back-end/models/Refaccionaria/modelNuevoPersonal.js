const mongoose=require('mongoose');

let Schema= mongoose.Schema;
let nuevoPersonalRefaccionaria= new Schema({
    nombrePersonal:{type:String},
    apepatPersonal:{type:String},
    apematPersonal:{type:String},
    edadPersonal:{type:Number},
    telefonoPersonal:{type:String},
    direccionPersonal:{type:String}
});

module.exports=mongoose.model('nuevoPersonal', nuevoPersonalRefaccionaria);