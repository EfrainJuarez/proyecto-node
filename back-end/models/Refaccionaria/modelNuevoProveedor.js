const mongoose=require('mongoose');

let Schema= mongoose.Schema;
let nuevoProveedorRefaccionaria= new Schema({
    nombreProveedor:{type:String},
    apepatProveedor:{type:String},
    apematProveedor:{type:String},
    empresaProveedor:{type:String},
    telefonoProveedor:{type:String},
    correoProveedor:{type:String},
    direccionProveedor:{type:String}
});

module.exports=mongoose.model('nuevoProveedor', nuevoProveedorRefaccionaria);