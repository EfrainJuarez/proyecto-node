const mongoose=require('mongoose');

let Schema= mongoose.Schema;
let nuevaSucursallRefaccionaria= new Schema({
    noSucursal:{type:String},
    direccionSucursal:{type:String},
    encargadoSucursal:{type:String},
    telefonoSucursal:{type:String},
    noEmpleadosSucursal:{type:Number},
});

module.exports=mongoose.model('nuevaSucursal', nuevaSucursallRefaccionaria);