const express=require('express');
const modelProveedorNuevo=require('../../models/Refaccionaria/modelNuevoProveedor');

let app =express();

app.post('/proveedor/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);

    let newSchemaProveedor=new modelProveedorNuevo({
        nombreProveedor:body.nombreProveedor,
        apepatProveedor:body.apepatProveedor,
        apematProveedor:body.apematProveedor,
        empresaProveedor:body.empresaProveedor,
        telefonoProveedor:body.telefonoProveedor,
        correoProveedor:body.correoProveedor,
        direccionProveedor:body.direccionProveedor
    });

    newSchemaProveedor
    .save()
    .then(
        (data)=>{
        return res.status(200)
        .json({
            ok:true,
            message:"Datos guardados",
            data
        });
    })
    .catch((err) =>{
        return res.status(500).json({
            ok:false,
            message:"Datos no guardados",
            data
        });
    });
})

//? Metodo GET
app.get('/obtener/proveedor',async (req,res)=> {
    //find es para encontrar todo el documento
    //existen diferentes tipos de find
    const respuesta = await modelProveedorNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});

//? Metodo UPDATE
app.put('/update/proveedor/:id',async (req,res) => {
    let id = req.params.id;
    //siempre que se declare una constante va a ir al require
    const campos = req.body;
    //Si no queremos actualizar todos, los eliminamos
    // delete campos.nombre;
    //En este caso serán todos
    //new:true es para que nos devuelva el registro ya actualizado
    const respuesta = await modelProveedorNuevo.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,
        msj:"Actualizado con éxito",
        respuesta
    })
});

//? Metodo Delete
app.delete('/delete/proveedor/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelProveedorNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"Eliminado con éxito",
        respuesta
    });
});

module.exports=app;