const express= require('express');
const app =express();

app.use(require('./text'));
app.use(require('./prodcutoNuevo/routeNuevoProducto'));
app.use(require('./personalNuevo/routeNuevoPersonal'));
app.use(require('./sucursalNueva/routeNuevoSucursal'));
app.use(require('./ProveedorNuevo/routeNuevoProveedor'));
module.exports=app;