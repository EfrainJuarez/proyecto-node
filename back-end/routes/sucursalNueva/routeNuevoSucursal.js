const express=require('express');
const modelSucursalNuevo=require('../../models/Refaccionaria/modelNuevaSucursal');

let app =express();

app.post('/sucursal/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);

    let newSchemaSucursal=new modelSucursalNuevo({
        noSucursal:body.noSucursal,
        direccionSucursal:body.direccionSucursal,
        encargadoSucursal:body.encargadoSucursal,
        telefonoSucursal:body.telefonoSucursal,
        noEmpleadosSucursal:body.noEmpleadosSucursal,
    });

    newSchemaSucursal
    .save()
    .then(
        (data)=>{
        return res.status(200)
        .json({
            ok:true,
            message:"Datos guardados",
            data
        });
    })
    .catch((err) =>{
        return res.status(500).json({
            ok:false,
            message:"Datos no guardados",
            data
        });
    });
})


//? Metodo GET
app.get('/obtener/sucursal',async (req,res)=> {
    //find es para encontrar todo el documento
    //existen diferentes tipos de find
    const respuesta = await modelSucursalNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});

//? Metodo UPDATE
app.put('/update/sucursal/:id',async (req,res) => {
    let id = req.params.id;
    //siempre que se declare una constante va a ir al require
    const campos = req.body;
    //Si no queremos actualizar todos, los eliminamos
    // delete campos.nombre;
    //En este caso serán todos
    //new:true es para que nos devuelva el registro ya actualizado
    const respuesta = await modelSucursalNuevo.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,
        msj:"Actualizado con éxito",
        respuesta
    })
});

//? Metodo Delete
app.delete('/delete/sucursal/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelSucursalNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"Eliminado con éxito",
        respuesta
    });
});


module.exports=app; 