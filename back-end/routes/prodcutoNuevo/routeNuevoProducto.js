const express=require('express');
const modelProductoNuevo=require('../../models/Refaccionaria/modelNuevoProducto');

let app =express();

app.post('/producto/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);

    let newSchemaProducto=new modelProductoNuevo({
        nombreProducto:body.nombreProducto,
        marcaProducto:body.marcaProducto,
        presntacionProducto:body.presntacionProducto,
        contenidoProducto:body.contenidoProducto,
        costoProducto:body.costoProducto,
        proveedorProducto:body.proveedorProducto,
        cantidadProducto:body.cantidadProducto,
        statusProducto:body.statusProducto,
        descripcionProducto:body.descripcionProducto
    });

    newSchemaProducto
    .save()
    .then(
        (data)=>{
        return res.status(200)
        .json({
            ok:true,
            message:"Datos guardados",
            data
        });
    })
    .catch((err) =>{
        return res.status(500).json({
            ok:false,
            message:"Datos no guardados",
            data
        });
    });
})


//? Metodo GET
app.get('/obtener/producto',async (req,res)=> {
    //find es para encontrar todo el documento
    //existen diferentes tipos de find
    const respuesta = await modelProductoNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});


app.get('/obtener/producto/:id',async (req,res)=> {
    //Declaramos el id
    let id =  req.params.id;
    //find es para encontrar todo el documento
    //existen diferentes tipos de find
    const respuesta = await modelProductoNuevo.findById(id);
    res.status(200).json({
        ok:true,
        respuesta
    });
});

//? Metodo UPDATE
app.put('/update/producto/:id',async (req,res) => {
    let id = req.params.id;
    //siempre que se declare una constante va a ir al require
    const campos = req.body;
    //Si no queremos actualizar todos, los eliminamos
    // delete campos.nombre;
    //En este caso serán todos
    //new:true es para que nos devuelva el registro ya actualizado
    const respuesta = await modelProductoNuevo.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,
        msj:"Actualizado con éxito",
        respuesta
    })
});

//? Metodo Delete
app.delete('/delete/producto/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelProductoNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"Eliminado con éxito",
        respuesta
    });
});

module.exports=app;