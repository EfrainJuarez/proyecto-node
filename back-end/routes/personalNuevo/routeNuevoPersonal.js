const express=require('express');
const modelPersonalNuevo=require('../../models/Refaccionaria/modelNuevoPersonal');

let app =express();

//? Metodo POST
app.post('/personal/nuevo', (req, res)=>{
    let body=req.body;
    console.log(body);

    let newSchemaPersonal=new modelPersonalNuevo({
        nombrePersonal:body.nombrePersonal,
        apepatPersonal:body.apepatPersonal,
        apematPersonal:body.apematPersonal,
        edadPersonal:body.edadPersonal,
        telefonoPersonal:body.telefonoPersonal,
        direccionPersonal:body.direccionPersonal
    });

    newSchemaPersonal
    .save()
    .then(
        (data)=>{
        return res.status(200)
        .json({
            ok:true,
            message:"Datos guardados",
            data
        });
    })
    .catch((err) =>{
        return res.status(500).json({
            ok:false,
            message:"Datos no guardados",
            data
        });
    });
})
//? Metodo GET
app.get('/obtener/personal',async (req,res)=> {
    //find es para encontrar todo el documento
    //existen diferentes tipos de find
    const respuesta = await modelPersonalNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });
});


//? Metodo UPDATE
app.put('/update/personal/:id',async (req,res) => {
    let id = req.params.id;
    //siempre que se declare una constante va a ir al require
    const campos = req.body;
    //Si no queremos actualizar todos, los eliminamos
    // delete campos.nombre;
    //En este caso serán todos
    //new:true es para que nos devuelva el registro ya actualizado
    const respuesta = await modelPersonalNuevo.findByIdAndUpdate(id,campos,{new:true});

    res.status(200).json({
        ok:true,
        msj:"Actualizado con éxito",
        respuesta
    })
});

//? Metodo Delete
app.delete('/delete/personal/:id',async (req,res)=>{
    let id = req.params.id;
    const respuesta = await modelPersonalNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj:"Eliminado con éxito",
        respuesta
    });
});



module.exports=app;